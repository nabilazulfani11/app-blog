const model = require('../models')
const bcrypt = require('bcrypt')
const passport = require('passport')
const session = require('express-session')

class Controller {
    static getUser (req, res){
        let data = []
        model.User.findAll()
        .then(users => {
            for(const newUser of users){
                console.log(newUser.dataValues)
                data.push(newUser.dataValues)
            }
            res.render('users', {
                data
            })
        })
    }

    //Setup Register
    static getRegister (re,res){
        res.render('register')
    }

    static register(req,res){
        const {username,password} = req.body
        const passwordEncrypted = bcrypt.hashSync(password, 10)
        model.User.create({
            username:username,
            password:passwordEncrypted
        }).then(
            res.redirect('/login')
        )
    }

    // Setup Login
    static getLogin (req,res){
        if(req.isAuthenticated()){
            res.redirect('/dashboard')
        }else{
            res.render('login')
        }
    }
    static postLogin(req,res){
        console.log('Masuk Login')
        const body = req.body
        console.log(body)
        passport.authenticate("local",{
            successRedirect : '/dashboard',
            failureFlash: '/login',
            failureFlash: true
        })(req, res);
    }
    static getDashboard(req,res){
        const username = req.user.username
        const password = req.user.password
        console.log(username)
        res.render('dashboard', {
            username
        })
    }

    //Setup Blog
    static getBlog (req,res){
        //const username = req.user.username
        console.log(`Ini Id req :${req.user.id}`)
        

        model.Tweet.findAll({
            where : {
                UserId:req.user.id

            },raw:true

        }).then(Tweets=>{
            console.log(Tweets)
            console.log (req.user.username)
            if (Tweets !== null){
                res.render('blog',{
                    Tweets:Tweets,
                    User :req.user.username
                })  
            }else{
                res.render('blog',{
                    Tweets:null
                })
            }
            
        })
        
    }
    static postBlog (req,res){
        const {title,description} = req.body
        const username = req.user.id
        
        //console.log (`Test :${username}`)
        model.Tweet.create({
            title:title,
            description:description,
            UserId:username
        }).then(()=>{
            model.Tweet.findAll({
                where : {
                    UserId:req.user.id
                    
                },raw:true
    
            }).then(Tweets=>{
                console.log(Tweets)
                console.log (req.user.username)
                if (Tweets !== null){
                    res.render('blog',{
                        Tweets:Tweets,
                        User:req.user.username

                    })  
                }else{
                    res.render('blog',{
                        Tweets:null
                    })
                }
                
            })
        }
            
        )
        
    }

    
}

module.exports = Controller