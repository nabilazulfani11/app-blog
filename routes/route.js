const express = require('express')
const router = express.Router()
const app = require('../server')
const controller = require('../controllers/controller')

const Authenticated = (req, res, next)=>{
    if(req.isAuthenticated()){
        next()
    }else{
        res.redirect('/login')
    }
}

router.get('/register', controller.getRegister)
router.post('/register', controller.register)
router.get('/login', controller.getLogin)
router.post('/login', controller.postLogin)
router.get('/dashboard',Authenticated,controller.getDashboard)
router.get('/blog', Authenticated, controller.getBlog)
router.post('/blog', controller.postBlog)

module.exports = router