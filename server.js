// Require Package yang akan dipakai
const express = require('express')
const app = express()
const port = process.env.PORT || 1998;
const router = require('./routes/route')
const path = require('path')
const session = require('express-session')
const flash = require('express-flash')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const model = require('./models')
const bcrypt = require('bcrypt')

//Setup folder Views
app.set('view engine', 'ejs')
app.set("views", path.join(__dirname,"views"))

//Setup session Handler
app.use(session({
    secret : 'Rahasia',
    resave: true,
    saveUninitialized: true
}))

//Setup folder Bosstrap
app.use(express.static(__dirname + '/assets'))

//setup passport Strategy
passport.use(
    new LocalStrategy (
    {
        usernameField: "username",
        passwordField: "password"
    },
    (username,password,done) =>{
        console.log("Masuk ke server")
        model.User.findOne({
            where : {
                username:username
            }
        }).then(User => {
            if (User.dataValues){
                if(bcrypt.compareSync(password,User.dataValues.password)){
                    return done (null, User)
                }else{
                    return done (null,false, {
                        message: 'Password Salah !'
                    })
                }
            }else{
                return done (null, false,{
                    message: 'Username Salah !'
                })
            }
        })
    }
 )
)

passport.serializeUser((user, done) => {
    done(null, user)
})

passport.deserializeUser((obj, done) => {
    done(null, obj)
})


app.use(passport.initialize())
app.use(passport.session())

app.use (flash())
app.use(express.json())

app.use(express.urlencoded({extended: false}))

app.use('/', router)

app.listen(port, () => {
    console.log(port)
})

module.exports = app